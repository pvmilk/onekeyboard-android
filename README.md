OneKeyboard
===========

This android keyboard is used as a proof of concept for a new Thai input system.
Originally, a Thai keyboard layout is used to type thai characters one at the
time. Then combining them into words and sentences. Here, we are trying to
develop a new input system which used an English keyboard layout. In other
words, a transliteration from English to Thai (or simply called  a karaoke typing in Thai).

Conceptually, the idea is to map words in the romanization space. Here, the
Royal Thai Romanaization system is used. However, other romanization system can
also be used.

Please be noted that the current version is in a very early state (ALPHA), and lots more
developements are needed in order to make the keyboard usable.

Preparation of the data
=======================

Please contact the author for the dictionary file that works with the program.

Milk Phongtharin
pvmilk@gmail.com
